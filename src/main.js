import { createApp } from 'vue';
import App from './App.vue';
import ele from './utils/electronTool.js';

const { render, main, electron, remote } = ele;

const app = createApp(App);

// app.config.globalProperties.$electron = electron;
// app.config.globalProperties.$remote = remote;

app.config.globalProperties.$render = render;
app.config.globalProperties.$main = main;
app.config.globalProperties.$electron = {
  render: electron,
  main: remote,
};

app.mount('#app');
