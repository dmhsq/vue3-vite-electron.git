# Vue 3 + Vite + electron

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

You can learn somethins about electron
[https://www.electronjs.org/](https://www.electronjs.org/)

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## Apis

### 直接使用 remote / electron

在 vue 文件中 使用 this.$electron.remote 可以获取 remote 包括主进程方法

使用 this.$electron.electron 中 可以调用渲染进程方法

### this.$main

目前包括：

createWindow(windowConfig:Object,url:string,divConfig:Object)

type windowConfig = {
width:xxx,
height:xxx,
...
// 参考 electron 文档的 [https://www.electronjs.org/zh/docs/latest/api/browser-window](https://www.electronjs.org/zh/docs/latest/api/browser-window)
}

```js
// windowConfig 可以自定义配置窗口配置 可不写 有默认
const windowConfig = {
  width: 1200,
  height: 800,
};
const url = 'https://xxxxx'; // 或者可以使用 file://来引用本地文件
const divConfig = {
  isDevTool: false, //是否开启窗口调试
  name: 'xxx', // 建议填写 可通过名字删除和获取窗口属性
};
this.$main.createWindow(windowConfig, url, divConfig);
```
