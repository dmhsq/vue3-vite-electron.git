const { app, Menu } = require('electron');
const isDev = require('electron-is-dev');
const AppWindow = require('./AppWindow.js');
const menuItems = require('./menu.config.js');
const path = require('path');
const config = require('../config/electron.config.js');

app.on('ready', () => {
  const mainWindowConfig = {
    width: 1440,
    height: 768,
  };
  const urlLocation = isDev
    ? 'http://localhost:3000'
    : `file://${path.join(__dirname, './index.html')}`;
  mainWindow = new AppWindow(mainWindowConfig, urlLocation, config.isDevTool);
  mainWindow.on('closed', () => {
    mainWindow = null;
    if (config.closeMianAll) {
      app.quit();
    }
  });
  require('@electron/remote/main').initialize();
  require('@electron/remote/main').enable(mainWindow.webContents);

  if (config.isOwnMenu) {
    const menu = Menu.buildFromTemplate(menuItems);
    Menu.setApplicationMenu(menu);
  }
});

app.on('window-all-closed', () => {
  app.quit();
});
